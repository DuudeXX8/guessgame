import React from "react";
import classes from "./Buttons.module.scss";

const Buttons = props => {

    const main = (fifty,sec,skip) => {
       if(fifty) fifty();
       if(sec) sec();
       if(skip) skip();
    }
    const {removeTwoVariant,plusTenSec,skipQuestion} = props;
    return (
        <span className={props.deactive ? classes.active : classes.losed}>
            <span onClick={_ => main(removeTwoVariant, plusTenSec, skipQuestion)} className={classes.ctaBtn}>
                <div className={classes.innerFill}>
                    <span>{props.children}</span>
                </div>
            </span>
        </span>
    )
}
export default Buttons;