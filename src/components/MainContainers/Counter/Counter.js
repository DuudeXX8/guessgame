import React, {Component} from 'react'; //,useState,useRef,useEffect,useCallback
import * as actionCreators from "../../../store/actions/actions";
import {connect} from "react-redux";

import classes from "./Counter.module.scss";

/*  This part of code need to rewrite in Hook way */

class Counter extends Component {

  static defaultProps = {
    time:10,
    secPatrule:false,
  }

  constructor(props){
    super(props);
    const {time,secPatrule} = this.props;
    this.state = {
      count:time,
      sec:secPatrule,
      res:props.result
    }
  }
  componentDidMount() {
      return this.props.gameOver === false ? this.startTimer() : null;
  }

  tick(){
    let {count} = this.state;

    let {
      stopTimer,
      secPatrule,
      toggleSecIfTrueVariant,
      decreaseTimer,
      plusAdditionalTenSec,
      tenSecondsAdded,
      tenAdded,
      start
    } = this.props;

    if(start === true){
      if (stopTimer === false) {
        // ------------------- CHECK if questions is not ended     -----------
        if (count > 0) {
          // ------------------- Check if counter more than zero     -----------
          if (secPatrule === false) {
            this.setState({count: (count - 1)});
            if (tenAdded === false) { // If help (+10 seconds)  not used go to this block
              if (plusAdditionalTenSec === false) {
                this.setState({count: (count = 10)});
                tenSecondsAdded();
              }
            }
          } else {
            toggleSecIfTrueVariant();
            this.setState({count: (count = 10)});
          }
          // ------------------- Check if counter less than zero        -----------
        } else {
          decreaseTimer(count);
        }
        // -------------------   Check if questions ended    -----------
      } else {
        return null;
      }
    }
  }

  startTimer() {
    this.timer = setInterval(this.tick.bind(this), 1000)
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {

    if(this.props.start === false){
        return (
          <div className={classes.mainscore}>
              <h1 className={classes.enumerator}>0</h1>
          </div>
      );
    } else {

      if(this.props.loosed === true || this.props.stopTimer === true){
        clearInterval(this.timer);
        return null;
      } else {
        return (
            <div className={classes.mainscore}>
                <h1 className={classes.enumerator}>{this.state.count}</h1>
            </div>
        );
      }
    }
  }
}

const mapStateToProps =  state => {
    return {
        secPatrule:state.root.addMoreSec, //If secPatrule true add 10 seconds to timer
        time: state.root.timer, // ------ countdown  10 seconds------- //
        loosed: state.root.losedRemoveTimer, //Remove countdown
        stopTimer: state.root.stopTimer, //Check if questions ended
        plusAdditionalTenSec: state.root.plusTen, //Help additional ten seconds
        tenAdded: state.root.tenSecondsAdded, //If help is used this will return true
        start: state.root.started,
        gameOver:state.root.gameOver
    };
}
const mapDispatchToProps = dispatch => {
    return {
        decreaseTimer:(count) => dispatch(actionCreators.deacrese(count)), //Decrease countdown
        toggleSecIfTrueVariant: _ => dispatch(actionCreators.toggleIftrue()),  //Add additional ten seconds if answer is true
        tenSecondsAdded: _ => dispatch(actionCreators.tenSecondsAdded()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);