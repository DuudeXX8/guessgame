import React, {useState, useEffect, useRef, useCallback} from "react";
import * as actionCreators from "../../../store/actions/actions";
import {
    connect
} from "react-redux";

import classes from "./TimeForGame.module.scss";


const TimeForGame = (props) => {
    const [seconds, setSeconds] = useState(props.seconds);
    const [minutes, setMinutes] = useState(props.minutes);
    const propsRef = useRef(props);
    useEffect(() => {
        propsRef.current = props;
    });

    // Update time with callback function
    const tick = useCallback(() => {
        if (propsRef.current.start === true) {
            setSeconds(s => {
                if (s >= 59) {
                    setMinutes(m => (m === 60 ? 0 : m + 1));
                return 0;
                }
                return s + 1;
            });
        }
    }, [setSeconds, setMinutes, propsRef]);

    // ref created one time now we can add it as dependency to hooks
    const timerRef = useRef();
    // create and stop timer in everytime as we need
    const startTimer = useCallback(() => {
        timerRef.current = setInterval(tick, 1000);
    }, [timerRef, tick]);
    const clearTimer = useCallback(() => clearInterval(timerRef.current), [timerRef]);

    useEffect(() => {
        if (propsRef.current.gameComplete === false && propsRef.current.gameOver === false) {
            startTimer();
        }
        return () => clearTimer();
    }, [propsRef, startTimer, clearTimer]);

    let {
        stopTimer,
        start,
        gameComplete
    } = props;

    if(gameComplete === false){
        // ----- IF GAME IS NOT PLAYED ONCE ------------- //
        if(start === false){
        // --- IF GAME IS NOT STARTED YET --------- //
            return (
                <h2 className={classes.timer}>00:00</h2>
            )
        } else {

            if(stopTimer === false){
                return (
                    <h2 className={classes.timer}>{minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</h2>
                )
            }  else {
                clearTimer();
                props.sendTimerResult(minutes, seconds);
                return (
                    <h2 className={classes.timer}>{minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</h2>
                )
            }
        }

    } else {
        return (
            <h2 className={classes.timer}>{minutes < 10 ? `0${minutes}` : minutes}:{seconds < 10 ? `0${seconds}` : seconds}</h2>
        )
    }
}


const mapStateToProps = state => {
    const {minutes,seconds,gameComplete} = state.fullTime;
    return {
        minutes,
        seconds,
        stopTimer:state.root.stopTimer,
        start: state.root.started,
        gameComplete,
        gameOver:state.root.gameOver,
    };
}
const mapDispatchToProps = dispatch => {
    return {
        sendTimerResult: (minutes, seconds) => dispatch(actionCreators.timeEnd(minutes, seconds)), //Send timer result to reducer when game is over
        decreaseTimer: (count) => dispatch(actionCreators.deacrese(count)), //Decrease counter
        toggleSecIfTrueVariant: _ => dispatch(actionCreators.toggleIftrue()) //Add more seconds if answer is true
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeForGame);