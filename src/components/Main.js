import React,{Component, Fragment} from "react";
import * as actionCreators from "../store/actions/actions";
import {connect} from "react-redux";
import {Container} from "reactstrap";


import classes from "./Main.module.scss";
import Counter from "./MainContainers/Counter/Counter";
import TimeForGame from "./MainContainers/TimeForGame/TimeForGame";
import Buttons from "./MainComponents/Buttons/Buttons";

import MainImg from "../assets/img/Gamelogos/logo.png";
import { GAME_IS_NOT_STARTED } from "./consts";

class Main extends Component {

    constructor(props) {
        super(props);
        this.imgSrcRef = React.createRef();
        this.timer = React.createRef();
    }

    renderListHandler(){
        let Result = this.props.isAnswerFalse;
        const {
            heroes,
            helpFiftyFifty,
            fiftyFiftyRandomNumbGen,
            random,
            start,
            message
        } = this.props;

        if(start === false){
            return (
                    <div className={classes.theGameIsNotStartedYet}>
                        <h4>{message  ? GAME_IS_NOT_STARTED : `Пожалуйста нажмите на кнопку старт для того чтобы начать игру!`}</h4>
                    </div>
                )
        }
        if(heroes){
            if(helpFiftyFifty){
                /**
                 * @method deleteFromArr will be remove in future this code below need only for demonstrate purposes,
                 * and Help for user 50 x 50 will be rewrite more elegant way with communication redux store 
                 * @module storage/components
                */
                function deleteFromArr(firstEl, secondEl, arr) {
                    if (firstEl === random && firstEl === 0) {
                        firstEl += 1;
                    } else if (firstEl === random && firstEl === 1) {
                        firstEl -= 1;
                    } else if (secondEl === random && secondEl === 2) {
                        secondEl += 1;
                    } else if (secondEl === random && secondEl === 3) {
                        secondEl -= 1;
                    }
                    delete arr[firstEl];
                    delete arr[secondEl];
                    return arr;
                }
                const newHeroes = deleteFromArr(fiftyFiftyRandomNumbGen(2), (fiftyFiftyRandomNumbGen(2) + 2), heroes);

                return newHeroes.map(hero => {

                    return (
                        <div className={classes.variant} key={hero.name}>
                            <h4 onClick={() => this.props.onClickOneOfVariant(hero.name,this.imgSrcRef)}>{hero.name}</h4>
                        </div>
                    )
                })
            } else if(helpFiftyFifty === false || helpFiftyFifty === null) {

                return heroes.map(hero => {

                    return (
                            <div className={classes.variant} key={hero.name}>
                                <h4 onClick={() => this.props.onClickOneOfVariant(hero.name,this.imgSrcRef)}>{hero.name}</h4>
                            </div>
                        )
                })
            }

        }   else {
            return <Result />
        }
    }

    startGameHandler(){

        const {
            heroes,
            random,
            start,
            correctAnswerCount,
            complexityLevel,
            removeTwoVariant,
            deactivedFifty,
            plusTenSec,
            additionalTenSec,
            skip,
            skipBool,
            onClickStart,
            gameBegins
        } = this.props;
        const arr = this.renderListHandler();
        // RENDER COMPONENT MULTIPLE TIMES //
        const times = (length, fn) => Array.from({ length }, (_, i) => fn(i));
        return (
                <Fragment>
                    <div className={classes.gamePlace}>
                        <div className={classes.mainGameInfo}>
                            <h3>Кто на фото?</h3>

                            <div className={classes.rules}>
                                <div onClick={() => this.gameStartTakeTwoFunctions(
                                                    onClickStart,
                                                    gameBegins)
                                                }
                                            className={start ? classes.passiveStart : classes.activeStart}>
                                    <h5>START</h5>
                                </div>
                            </div>

                            <div className={classes.rules}>
                                <span>Угадано: {start === false ? "0" : correctAnswerCount}</span>
                            </div>
                            <div className={classes.rules}>
                                <span>Сложность: {start === false ? "1" :  complexityLevel}</span>
                            </div>

                            <TimeForGame />
                        </div>

                        {start && heroes  ? (<div className={classes.heroImageContainer}>
                                                {times(4, i => <span key={i}></span>)}
                                                <img
                                                    data-alias={btoa(heroes[random].name)}
                                                    className={classes.guessWho}
                                                    ref={this.imgSrcRef}
                                                    src={heroes[random].src}
                                                    alt="secret" />
                                            </div>) : (<div className={classes.heroImageContainer}>
                                                                <img
                                                                    className={classes.guessWho}
                                                                    src={MainImg}
                                                                    alt="Logo" />
                                                        </div>)}

                        <div className={classes.hint}>
                            <h3>Три Подсказки:</h3>
                            <div className={classes.hintList}>

                                <Buttons
                                    deactive={deactivedFifty}
                                    removeTwoVariant={removeTwoVariant}>
                                        {deactivedFifty ? "50x50" : <span role="img" aria-label="close" >&#10060;</span>}
                                </Buttons>

                                <Buttons
                                    plusTenSec={plusTenSec}
                                    deactive={additionalTenSec}>
                                        {additionalTenSec ? `+ 10 секунд` : <span role="img" aria-label="close" >&#10060;</span>}
                                </Buttons>

                                <Buttons
                                    skipQuestion={skip}
                                    deactive={skipBool}>
                                    {skipBool ? `пропустить` : <span role="img" aria-label="close" >&#10060;</span>}</Buttons>
                            </div>
                            <Counter result={false}/>  {/* Timer*/}
                        </div>

                    </div>
                    <div className={classes.listofanswers}>{arr}</div>
                </Fragment>
            )
    }
    gameStartTakeTwoFunctions(handleFirst, handleSecond) {
        handleFirst();
        handleSecond();
    }

    componentWillUnmount(){
        /* -------- IF GAME IS STARTED AND ROUTE IS CHANGING THEN GAME OVER -------*/
        const {start,heroes,gameOverRouteChanged} = this.props;
        if(start && heroes !== null){
            gameOverRouteChanged();
        }
    }

    render(){
        return (
            <Container className="h-100">
                <div className={classes.mainElement}>
                    {this.props.gameOver ? "You changed route game is over!" : this.startGameHandler()}
                </div>
            </Container>
        )
    }
}

/**
 * CONNECTING STATE TO GLOBAL FOR MAIN
*/

const mapStateToProps =  state => {
    return {
        heroes: state.root.nameAndImage,
        isAnswerFalse: state.root.trueOrFalse,
        start: state.root.started,
        random: state.root.randomImageGen,
        stopTimer: state.root.stopTimer,
        correctAnswerCount: state.root.correctAnswer,
        complexityLevel: state.root.lvlComplexity,
        helpFiftyFifty: state.root.helpRemoveTwoAnswers,
        fiftyFiftyRandomNumbGen:state.root.fromZero,
        deactivedFifty: state.root.deactivedFifty,
        additionalTenSec: state.root.deactivedTen,
        skipBool:state.root.skip,
        message:state.root.message,
        gameOver:state.root.gameOver
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onClickStart: () =>  dispatch(actionCreators.gameIsStarted()), // TO START THE GAME
        onClickOneOfVariant:(value,{current}) => dispatch(actionCreators.trueVariant(value,current.dataset.alias)), //If clicked one of variants run this
        gameBegins: () => dispatch(actionCreators.watchTimer()),
        removeTwoVariant:() => dispatch(actionCreators.halfOfAnswers()),
        plusTenSec: () => dispatch(actionCreators.plusTenSec()),
        skip:() => dispatch(actionCreators.skipQuestion()),
        gameOverRouteChanged:() => dispatch(actionCreators.gameOver())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);