export const TRUEVARIANT = "TRUEVARIANT";
export const GAMEISSTARTED = "GAMEISSTARTED";
export const WATCHTIMER = "WATCHTIMER";
export const DECREASE = "DECREASE";
export const TOGGLERBOOLEAN = "TOGGLERBOOLEAN";
export const TIMEEND = "TIMEEND";
export const HALFOFANSWERS = "HALFOFANSWERS";
export const PLUSTENSEC = "PLUSTENSEC";
export const TENSECONDSADDED = "TENSECONDSADDED";
export const SKIPQUESTION = "SKIPQUESTION";
export const GAMEOVER = "GAMEOVER";

export const timeEnd = (minute, second) => {

    return {
        type: TIMEEND,
        timerSec: second,
        timerMin: minute,
    };
};

export const trueVariant = (value, src) => {
    return {
        type: TRUEVARIANT,
        payload: value,
        imageSrc: src,
    };
};

export const gameIsStarted = _ => {
    return {
        type: GAMEISSTARTED
    }
}

export const deacrese = (count) => {
    return {
        type: DECREASE,
        payload: count
    }
}

export const watchTimer = _ => {
    return {
        type: WATCHTIMER
    }
}
export const toggleIftrue = _ => {
    return {
        type: TOGGLERBOOLEAN
    }
}
export const halfOfAnswers = _ => {
    return {
        type: HALFOFANSWERS
    }
}
export const plusTenSec = _ => {
    return {
        type: PLUSTENSEC
    }
}
export const tenSecondsAdded = _ => {
    return {
        type: TENSECONDSADDED
    }
}
export const skipQuestion = _ => {
    return {
        type: SKIPQUESTION
    }
}
export const gameOver = _ => {
    return {
        type: GAMEOVER
    }
}