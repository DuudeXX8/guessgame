import * as actionTypes from "../actions/actions";

import TimeEnd from "../../components/MainComponents/TimeEnd/TimeEnd";

import False from "../../components/MainComponents/False/False";
import YouWin from "../../components/MainComponents/YouWin/YouWin";
import {shuffle,randomImageGenerator} from "../helpers/helpers";

// ----------- Currently Importing Data this way but this will be improving in future ------------- //
import {
    data,
    dataForLvlTwo
} from "../../fakeData/fakeData";

let rememberNumb;
let startNumb = 0,
    finishNumb = 4,
    countdown = 10,
    correctAnswer = 0,
    lvlComplexity = 1,
    initialData = data,
    levelTwo = dataForLvlTwo,
    getDataFromArr = 0;

const mainArrOfLevels = [initialData, levelTwo];

// -------Calculate length of all arrays inside of main array ->  mainArrOfLevels------ //
const finalDataLength = mainArrOfLevels.reduce((prev, next) => {
    return prev + (next.length - 4);
}, 0);

// ------- Shuffle all level arrays data ------ //
mainArrOfLevels.forEach(dataArr => {
    shuffle(dataArr);
});

const initialState = {
    nameAndImage: mainArrOfLevels[getDataFromArr].slice(startNumb, finishNumb),
    trueOrFalse: null,
    started: false,
    timer: countdown,
    randomImageGen: rememberNumb = randomImageGenerator(4),
    addMoreSec: false,
    losedRemoveTimer: false,
    stopTimer: false,
    helpRemoveTwoAnswers: false,
    deactivedFifty: true,
    fromZero: randomImageGenerator,
    plusTen: true,
    deactivedTen: true,
    skip: true,
    tenSecondsAdded: false,
    message: null,
    gameOver:false,
    correctAnswer,
    lvlComplexity,
}

/*
    --REDUCER IS WORKING SYNCHROUNOUS
    --SO NO ASYNCHRONOUS  CODE THERE NO SETTIMEOUT NO PROMISE NO ASYNC
*/

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GAMEISSTARTED:
            return {
                ...state,
                started: true,
            }
            //  ---------- TRUEVARIANT  ------------ //
        case actionTypes.TRUEVARIANT:
            const source = atob(action.imageSrc);
            // ------------ IF ANSWER IS TRUE ------------ //
            if (action.payload === source) {
                // ------------ IF QUESTIONS IS -- NOT -- ENDED -----------
                if (((state.correctAnswer * 4) + 5) <= finalDataLength) {

                    // ------------ ЕСЛИ ПРАВИЛЬНЫЕ ОТВЕТЫ МЕНЬШЕ 10  ------------
                    if (state.correctAnswer % 10 || state.correctAnswer === 0) {
                        return {
                            ...state,
                            addMoreSec: true,
                            randomImageGen: rememberNumb = randomImageGenerator(4),
                            helpRemoveTwoAnswers: null,
                            correctAnswer: correctAnswer += 1,
                            nameAndImage: mainArrOfLevels[getDataFromArr].slice(startNumb += 4, finishNumb += 4),
                        }
                        /* ------------ ЕСЛИ ПРАВИЛЬНЫЕ ОТВЕТЫ БОЛЬШЕ 10  УВЕЛИЧИВАЕМ СЛОЖНОСТЬ ------------
                        -- ОБНУЛЯЕМ ПЕРЕМЕННЫЕ И БЕРЕМ СЛЕДУИЩИЙ ЕЛЕМЕНТ МАССИВА
                        */
                    } else {
                        getDataFromArr += 1;
                        startNumb = 0;
                        finishNumb = 4;
                        return {
                            ...state,
                            randomImageGen: rememberNumb = randomImageGenerator(4),
                            addMoreSec: true,
                            correctAnswer: correctAnswer += 1,
                            nameAndImage: mainArrOfLevels[getDataFromArr].slice(startNumb += 4, finishNumb += 4),
                            lvlComplexity: lvlComplexity += 1,
                        }
                    }
                } else {
                    // ------------ IF QUESTIONS IS ENDED ------------
                    return {
                        ...state,
                        correctAnswer: correctAnswer += 1,
                        nameAndImage: null,
                        deactivedFifty: false,
                        deactivedTen: false,
                        skip: false,
                        started: true, //For Active button START
                        trueOrFalse: YouWin,
                        losedRemoveTimer: true, //Remove Countdown
                        stopTimer: true, // IF questions is ended stop and return timer
                    }
                }
            } else {
                // ------------ IF ANSWER IS FALSE ------------
                return {
                    ...state,
                    nameAndImage: null,
                    // ---- TURN OF 3 HELPS ----//
                    deactivedFifty: false,
                    deactivedTen: false,
                    skip: false,
                    // ---- TURN OF 3 HELPS ----//
                    correctAnswer,
                    started: true, //For Active button START
                    trueOrFalse: False,
                    losedRemoveTimer: true, //Remove Countdown
                    stopTimer: true, // If question is not true remove countdown
                }
            }
        case actionTypes.TOGGLERBOOLEAN:
            return {
                ...state,
                addMoreSec: false,
            }
            case actionTypes.DECREASE:
                return {
                    ...state,
                    // ---- TURN OF 3 HELPS ----//
                    deactivedFifty: false,
                        deactivedTen: false,
                        skip: false,
                        // ---- TURN OF 3 HELPS ----//
                        lvlComplexity,
                        stopTimer: true,
                        correctAnswer,
                        trueOrFalse: TimeEnd,
                        addMoreSec: false,
                        nameAndImage: null,
                        started: true, //For Active button START
                        losedRemoveTimer: true, //Remove Countdown
                }
        // ------ MAIN 3 HELPS Logic----- //
        case actionTypes.HALFOFANSWERS:

            if (state.started) {
                return {
                    ...state,
                    randomImageGen: rememberNumb,
                    fromZero: randomImageGenerator,
                    helpRemoveTwoAnswers: true,
                    deactivedFifty: false,
                }
            } else {
                return {
                    ...state,
                    message: true
                };
            }
        case actionTypes.PLUSTENSEC:
            if (state.started) {
                return {
                    ...state,
                    randomImageGen: rememberNumb,
                    plusTen: false,
                    deactivedTen: false,
                }
            } else {
                return {
                    ...state,
                    message: true
                }
            }
        case actionTypes.TENSECONDSADDED:
            if (state.started) {
                return {
                    ...state,
                    tenSecondsAdded: true,
                    randomImageGen: rememberNumb,
                }
            } else {
                return {
                    ...state,
                    message: true
                }
            }
        case actionTypes.SKIPQUESTION:
            if (state.started) {
                return {
                    ...state,
                    skip: false,
                    nameAndImage: mainArrOfLevels[getDataFromArr].slice(startNumb += 4, finishNumb += 4),
                }
            } else {
                return {
                    ...state,
                    message: true
                }
            }
        case actionTypes.GAMEOVER:
            return {
                ...state,
                gameOver:true, // IF USER CHANGE PAGE THEN GAME IS OVER
                nameAndImage:null,
            }
        default:
            return state;
    }
}
export default rootReducer;