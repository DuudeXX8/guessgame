import * as actionTypes from "../actions/actions";


const initialState = {
    minutes: 0,
    seconds: 0,
    //FOR TIMER COMPONENT WHEN TIME IS END 
    timeEndSecond: null,
    timeEndMinute: null,
    // ------ IF GAME IS COMPLETE CHECK THIS VALUE AND RETURN MINUTE AND SECOND
    gameComplete:false,
}
/*
    --REDUCER IS WORKING SYNCHROUNOUS
    --SO NO ASYNCHRONOUS  CODE THERE NO SETTIMEOUT NO PROMISE NO ASYNC
*/
const fullTime = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.WATCHTIMER:
            return {
                ...state,
            }
            case actionTypes.TIMEEND:
                return {
                    ...state,
                    minutes: action.timerMin,
                    seconds: action.timerSec,
                    gameComplete:true,
                }
                default:
                    return state;
    }
}
export default fullTime;