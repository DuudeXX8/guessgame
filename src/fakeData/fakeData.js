
import Vers from "../assets/img/heroeList/Captain Marvel.jpg";
import Venom from "../assets/img/heroeList/Venom.jpg";
import Thanos from "../assets/img/heroeList/Thanos.jpg";
import Logan from "../assets/img/heroeList/Wolverine.jpg";

import Spiderman from "../assets/img/heroeList/Spiderman.jpg";
import Redhulk from "../assets/img/heroeList/Red Hulk.jpg";
import Galactus from "../assets/img/heroeList/Galactus.jpg";
import Deadpool from "../assets/img/heroeList/Deadpool.jpg";

import Abomination from "../assets/img/heroeList/Abomination.jpg";
import AntMan from "../assets/img/heroeList/Antman.jpg";
import BlackPanther from "../assets/img/heroeList/Black Panther.jpg";
import BlackWidow from "../assets/img/heroeList/Black Widow.jpg";

import CaptainAmerica from "../assets/img/heroeList/Captain America.jpg";
import Carnage from "../assets/img/heroeList/Carnage.jpg";
import CorvusGlaive from "../assets/img/heroeList/Corvus Glaive.jpg";
import CullObsidian from "../assets/img/heroeList/Cull Obsidian.jpg";

import Destroyer from "../assets/img/heroeList/Destroyer.jpg";
import DoctorStrange from "../assets/img/heroeList/Doctor Strange.jpg";
import Drax from "../assets/img/heroeList/Drax.jpg";
import EbonyMaw from "../assets/img/heroeList/Ebony Maw.png";

import Gamora from "../assets/img/heroeList/Gamora.jpg";
import Groot from "../assets/img/heroeList/Groot.jpg";
import IronMan from "../assets/img/heroeList/Ironman.jpg";
import IronPatriot from "../assets/img/heroeList/Ironpatriot.jpg";

import Loki from "../assets/img/heroeList/Loki.jpg";
import Mantis from "../assets/img/heroeList/Mantis.jpg";
import Nebula from "../assets/img/heroeList/Nebula.jpg";
import NickFury from "../assets/img/heroeList/Fury.png";

import Odin from "../assets/img/heroeList/Odin.jpg";
import PepperPots from "../assets/img/heroeList/Pepper Potts.png";
import ProximaMidnight from "../assets/img/heroeList/Proxima Midnight.jpg";
import RedSkull from "../assets/img/heroeList/Red Skull.jpg";

import RocketRaccoon from "../assets/img/heroeList/Rocket Raccoon.jpg";
import Ronan from "../assets/img/heroeList/Ronan.jpg";
import StarLord from "../assets/img/heroeList/Starlord.jpg";
import Thor from "../assets/img/heroeList/Thor.jpg";

import Ultron from "../assets/img/heroeList/Ultron.jpg";
import Vision from "../assets/img/heroeList/Vision.jpg";
import Wasp from "../assets/img/heroeList/Wasp.png";
import x23 from "../assets/img/heroeList/X-23.jpg";

import Beast from "../assets/img/heroeList/Beast.png";
import Azazel from "../assets/img/heroeList/Azazel.jpg";
import NightCrawler from "../assets/img/heroeList/Night Crawler.jpg";
import Surtur from "../assets/img/heroeList/Surtur.jpg";

import YonRogg from "../assets/img/heroeList/Yon-Rogg.jpg";
import Talos from "../assets/img/heroeList/Talos.jpg";
import SandMan from "../assets/img/heroeList/Sand Man.jpg";
import Falcon from "../assets/img/heroeList/Falcon.jpg";

// ------------ LEVEL 2 -----------------//
import JeanGray from "../assets/img/heroeList/level-2/Jean Gray.jpg";
import Magneto from "../assets/img/heroeList/level-2/Magneto.jpg";
import Legion from "../assets/img/heroeList/level-2/Legion.png";
import Minerva from "../assets/img/heroeList/level-2/Minerva.jpg";

import Hulk from "../assets/img/heroeList/level-2/Hulk.jpg";
import Hela from "../assets/img/heroeList/level-2/Hela.jpg";
import Apocalypse from "../assets/img/heroeList/level-2/Apocalypse.png";
import GwenSpider from "../assets/img/heroeList/level-2/Gwen Spider.jpg";

import MRFantastic from "../assets/img/heroeList/level-2/Mr.Fantastic.jpg";
import Thing from "../assets/img/heroeList/level-2/Thing.jpg";
import BlackCat from "../assets/img/heroeList/level-2/Black Cat.jpg";
import GhostRider from "../assets/img/heroeList/level-2/Ghost Rider.jpg";



export const dataForLvlTwo = [{
    // -------------- 1 ----------------
    src: JeanGray,
    name: "Jean Gray",
}, {
    src: Magneto,
    name: "Magneto",
}, {
    src: Legion,
    name: "Legion",
}, {
    src: Minerva,
    name: "Minerva",
    // -------------- 2 ----------------
}, {
    src: Hulk,
    name: "Hulk",
}, {
    src: Hela,
    name: "Hela",
}, {
    src: Apocalypse,
    name: "Apocalypse",
}, {
    src: GwenSpider,
    name: "Gwen Spider",
    // -------------- 3 ----------------
}, {
    src: GhostRider,
    name: "Ghost Rider",
}, {
    src: BlackCat,
    name: "Black Cat",
}, {
    src: Thing,
    name: "Thing",
}, {
    src: MRFantastic,
    name: "Mr.Fantastic",
}, ]

export const data = [{
    // -------------- 1 ----------------
    src: Vers,
    name: "Captain Marvel",

}, {
    src: Venom,
    name: "Venom",
}, {
    src: Thanos,
    name: "Thanos",
}, {
    src: Logan,
    name: "Wolverine",
    // -------------- 2 ----------------
}, {
    src: Spiderman,
    name: "Spiderman",
}, {
    src: Redhulk,
    name: "Red Hulk",
}, {
    src: Galactus,
    name: "Galactus",
}, {
    src: Deadpool,
    name: "Deadpool",
    // -------------- 3 ----------------
}, {
    src: BlackWidow,
    name: "Black Widow",
}, {
    src: BlackPanther,
    name: "Black Panther",
}, {
    src: AntMan,
    name: "Antman",
}, {
    src: Abomination,
    name: "Abomination",
    // -------------- 4 ----------------
}, {
    src: CaptainAmerica,
    name: "Captain America",
}, {
    src: Carnage,
    name: "Carnage",
}, {
    src: CorvusGlaive,
    name: "Corvus Glaive",
}, {
    src: CullObsidian,
    name: "Cull Obsidian",
    // -------------- 5 ----------------
}, {
    src: Destroyer,
    name: "Destroyer",
}, {
    src: DoctorStrange,
    name: "Doctor Strange",
}, {
    src: Drax,
    name: "Drax",
}, {
    src: EbonyMaw,
    name: "Ebony Maw",
    // -------------- 6 ----------------
}, {
    src: Gamora,
    name: "Gamora",
}, {
    src: Groot,
    name: "Groot",
}, {
    src: IronMan,
    name: "Ironman",
}, {
    src: IronPatriot,
    name: "Ironpatriot",
    // -------------- 7 ----------------
}, {
    src: Loki,
    name: "Loki",
}, {
    src: Mantis,
    name: "Mantis",
}, {
    src: Nebula,
    name: "Nebula",
}, {
    src: NickFury,
    name: "Fury",
    // -------------- 8----------------
}, {
    src: Odin,
    name: "Odin",
}, {
    src: PepperPots,
    name: "Pepper Potts",
}, {
    src: ProximaMidnight,
    name: "Proxima Midnight",
}, {
    src: RedSkull,
    name: "Red Skull",
    // -------------- 9 ----------------
}, {
    src: RocketRaccoon,
    name: "Rocket Raccoon",
}, {
    src: Ronan,
    name: "Ronan",
}, {
    src: StarLord,
    name: "Starlord",
}, {
    src: Thor,
    name: "Thor",
    // -------------- 10 ----------------
}, {
    src: Ultron,
    name: "Ultron",
}, {
    src: Vision,
    name: "Vision",
}, {
    src: Wasp,
    name: "Wasp",
}, {
    src: x23,
    name: "X-23",
}, {
    // -------------- 11 ----------------
    src: Surtur,
    name: "Surtur",
}, {
    src: NightCrawler,
    name: "Night Crawler",
}, {
    src: Azazel,
    name: "Azazel",
}, {
    src: Beast,
    name: "Beast",
    // -------------- 12 ----------------
}, {
    src: YonRogg,
    name: "Yon-Rogg",
}, {
    src: Talos,
    name: "Talos",
}, {
    src: SandMan,
    name: "Sand Man",
}, {
    src: Falcon,
    name: "Falcon",
}, ]
