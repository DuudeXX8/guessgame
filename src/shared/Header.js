import React from "react";
import {Link} from "react-router-dom";
import styles from "./Header.module.scss";

import Logo from "./../assets/img/logo.png";

const Header = _ => {
    const headerLinkPaths = {
        local:"Main",
        about:"About",
        comics:"Comics",
        characters:"Characters"
    }
    return (
        <nav>
            <div className={styles.logo}>
                <Link to="/">
                    <img src={Logo} alt="Logo" />
                </Link>
            </div>
            <ul>
                {
                    Object.keys(headerLinkPaths).map(el => (
                        <li className={styles.btn} key={el}>
                            <svg width="100%" height="100%">
                                <rect width="100%" height="100%" pathLength="100"/>
                            </svg>
                            <Link className={styles.label} to={el === "local" ? "/" : el}>{el === "local" ? "Main" : headerLinkPaths[el]}</Link> 
                        </li>
                    ))
                }
            </ul>
        </nav>
    )
}

export default Header;  