import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter  as Router } from "react-router-dom";//BrowserRouter
import App from './App';

import {Provider } from "react-redux";
import {
    createStore,
    applyMiddleware,
    compose,
    combineReducers
} from "redux";
/*----- Redux thunk will be used when API is connected from backend ------------- */
import thunk from "redux-thunk";


import counterReducer from "./store/reducers/counter";
import rootReducer from "./store/reducers/mainReducer";
import fullTimeReducer from "./store/reducers/fulltime";

const mainReducer = combineReducers({
    ctr: counterReducer,
    root: rootReducer,
    fullTime: fullTimeReducer,
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(mainReducer, composeEnhancers(applyMiddleware(thunk)));


ReactDOM.render(<Provider {...{store}}>
                    <Router>
                        <App />
                    </Router>
                </Provider>, document.getElementById('root'));


