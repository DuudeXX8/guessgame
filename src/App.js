import React,{lazy, Suspense} from 'react';
import Header from "./shared/Header";
import {
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import "./styles/_normalize.scss";

const About = lazy(_ => import("./components/About"));
const Comics = lazy(_ => import("./components/Comics"));
const Characters = lazy(_ => import("./components/Characters"));
const Main = lazy(_ => import("./components/Main"));

function App() {
   const headerLinkPaths = {
     local: "Main",
     about: "About",
     comics: "Comics",
     characters: "Characters"
   }
  const routePaths = {
        local:<Main />,
        about:<About />,
        comics:<Comics />,
        characters:<Characters />
    }
  return (
    <>
      <Header />
      <Switch>
          {
              Object.keys(routePaths).map(el => (
                  <Route key={el} exact path={headerLinkPaths[el] === "Main" ? "/" : `/${headerLinkPaths[el]}`} render={_ => (
                      <Suspense fallback={<div>...Loading</div>}>{routePaths[el]}</Suspense>
                  )} />
              ))
          }
          <Redirect to="/" />
      </Switch>
    </>
  );
}

export default App;
